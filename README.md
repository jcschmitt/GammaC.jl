# GammaC

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://wistell.gitlab.io/GammaC.jl/dev)
[![Build Status](https://gitlab.com/WISTELL/GammaC.jl/badges/master/pipeline.svg)](https://gitlab.com/WISTELL/GammaC.jl/pipelines)
[![Coverage](https://gitlab.com/WISTELL/GammaC.jl/badges/master/coverage.svg)](https://gitlab.com/WISTELL/GammaC.jl/commits/master)
