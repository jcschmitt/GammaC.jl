module GammaC
using VMEC
using MPI

include("src/Functions.jl")
include("src/Interfaces.jl")

zetaMin = 0.0
zetaMax = 0.0
zetaStep = 0.0
BResolution = 1
epsEff = true
gamma_c = true

function setup(parameters::Dict{Symbol,Any})
  global zetaMin = haskey(parameters,:zetaMin) ? parameters[:zetaMin] : 0.0
  global zetaMax = haskey(parameters,:zetaMax) ? parameters[:zetaMax] : 0.0
  global zetaStep = haskey(parameters,:zetaStep) ? parameters[:zetaStep] : 0.0
  global BResolution = haskey(parameters,:BResolution) ? parameters[:BResolution] : 1
  global epsEff = haskey(parameters,:epsEff) ? parameters[:epsEff] : true
  global gamma_c = haskey(parameters,:gamma_c) ? parameters[:gamma_c] : true
  return nothing
end

function computeTarget(args::Dict{Symbol,Any},vmecSurface::VmecSurface{N,T},comm::MPI.Comm) where {N,T}
  setup(args)
  ϵγ = GammacTarget(vmecSurface,zetaMin,zetaStep,zetaMax,BResolution,comm,epsEff,gamma_c)
  return ϵγ
end

end
