using GammaC
using Documenter

DocMeta.setdocmeta!(GammaC, :DocTestSetup, :(using GammaC); recursive=true)

makedocs(;
    modules=[GammaC],
    authors="Benjamin Faber <bfaber@wisc.edu> and contributors",
    repo="https://gitlab.com/WISTELL/GammaC.jl/blob/{commit}{path}#{line}",
    sitename="GammaC.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://wistell.gitlab.io/GammaC.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
        "Library" => [
                      "Private" => "library/internals.md",
                     ],
    ],
)
