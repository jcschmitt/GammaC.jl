## Interfaces
```@autodocs
Modules = [GammaC]
Public = false
Pages = ["Interfaces.jl"]
```

## Functions
```@autodocs
Modules = [GammaC]
Public = false
Pages = ["Functions.jl"]
```
