# This script will show how to load a VMEC file using the Julia interface
# and how to query some quantities of interest
# The first section will load the vmec file and the remaining
# sections will calculate epsilon_effective and gamma_c on a given flux surface

import Pkg
Pkg.activate(".")
Pkg.instantiate()

using MPI

MPI.Init()
comm = MPI.COMM_WORLD
mpiRank=MPI.Comm_rank(comm)


# Set the path to the vmec WOUT file.  The user will need to change the path below
woutPath = "/ssd1/Wistell/configs/chup/wout_chup.nc"

#load dependencies
using NetCDF, VMEC, GammaC

#load in the raw VMEC data from NETCDF
wout = NetCDF.open(woutPath);

# load the VMEC data into two different formats
# "vmecdata" is a straightforward reconstruction of the vmec wout file but in a julia struct
# "vmec" is a processed form in which all relevant quantities are calculated as splines over the radial coordinate
vmec, vmecdata = VMEC.readVmecWout(wout);

#the stuff included up to this point is necessary for anything below
#the following sections, separated by dashed comment lines
#can be separated out individually and pasted into repl as desired

#--------------------------------------------------------
#Section 1: 
# calculate eps_effective at a specific flux surface
#
# s surface to calculate on
s = 0.3 

#calculate a surface quantity
vmecSurface = VmecSurface(s, vmec);

# Eps_effective requires two resolution parameters and a length along the field line
# good values for these depend on the config, so some convergence testing is needed
# similar configurations can probably use the same values
lMax = 100*π;
lRes = 2*π/1024;
bRes = 100;


# Without MPI
output = GammaC.ComputeGammac(vmecSurface, 0.0, lRes, lMax, bRes, true, true)

if mpiRank == 0
  println("output NoMPI: ",output);
end 

# With MPI
output2 = GammaC.ComputeGammac(vmecSurface, 0.0, lRes, lMax, bRes, comm, true, true)


if mpiRank == 0
  println("output W/MPI: ",output2);
end 





