"""
    GammacTarget(surfaces, vmec, zetaMin, zetaStep, zetaMax, BResolution, epsEff, gammaC)
    GammacTarget(surfaces, vmec, zetaMin, zetaStep, zetaMax, BResolution, comm, epsEff, gammaC)
    GammacTarget(vmecSurface, zetaMin, zetaStep, zetaMax, BResolution, epsEff, gammaC)
    GammacTarget(vmecSurface, zetaMin, zetaStep, zetaMax, BResolution, comm, epsEff, gammaC)

Function to calculate ``\\Gamma_c`` from one or several surfaces (with or without MPI)

# Arguments
 - `surfaces::Union{Float64, Vector{Float64}}`: a single float or vector of floats giving the surface at which to compute ``\\Gamma_c``. These values are given in normalized toroidal flux, ``s``
 - `vmec::VMEC.VmecData`: A Vmec struct
 - `vmecSurface::VmecSurface`: A VMEC surface file. Code requires either a vmec struct and 1 or more s values, or the vmec Surface
 - `zetaMin::Float64`: The toroidal angle value to start the calculation at, in radians
 - `zetaStep::Float64`: The step size to representing the resolution for the calculation, given in radians
 - `zetaMax::Float64`: The toroidal angle to end the calculation at, in radians
 - `BResolution::Int64`: The resolution for the B field integration
 - `comm::MPI:Comm`: If included the code will use the MPI interface to speed computation
 - `epsEff::Bool=true`: Optional argument default true. if true, code will calculate and return the value of ``\\epsilon_\\mathrm{eff}``
 - `gammac::Bool=true`: Optional argument default true. if true, code will calculate and return the value of ``\\Gamma_c``

# Outputs:
 - `targetValues::Vector{Float64}`: an array of values computing ``\\epsilon_\\mathrm{eff}`` and/or ``\\Gamma_c`` depending on the optional bool arguments. In case both are present (default) they are returned in the form in pairs of ``(\\epsilon_\\mathrm{eff}, \\Gamma_c)``
"""
# <<<<<<< HEAD
# function GammacTarget(surfaces::Union{Float64,Vector{Float64}},zetaMin::Float64, zetaStep::Float64, zetaMax::Float64,
#                       vmec::Vmec, BResolution::Int64, epsEff::Bool=true, gammac::Bool=true) where T
#   zetas = collect(zetaMin:zetaStep:zetaMax)
#   print("<---1Zeta goes from ", zetaMin, " to ", zetaMax, " in ", zetaStep, "-sized steps \n") 
#   nZeta = length(zetas)
#   alphas = [0.0] #can set this as optional input, but it's really not necessary
# =======
function GammacTarget(surfaces::Union{Float64,Vector{Float64}}, vmec::Vmec, zetaMin::Float64, 
                      zetaStep::Float64, zetaMax::Float64,
                      BResolution::Int64, epsEff::Bool=true, gammac::Bool=true)
# >>>>>>> wistell/GammaC.jl-master
  
  nTargetValues = length(surfaces)
  if epsEff && gammac
    targetValues = Array{Tuple{Float64, Float64}}(undef, nTargetValues)
  elseif epsEff || gammac
    targetValues = Array{Float64,1}(undef,nTargetValues)
  else
    println("must select either epsEff or gammac")
  end
  #println(nTargetValues)

  for index = 1:nTargetValues
    vmecSurface = VmecSurface(surfaces[index],vmec)

    targetValues[index] = ComputeGammac(vmecSurface,zetaMin, zetaStep, zetaMax, BResolution,epsEff,gammac) 
  end
  return targetValues
end

function GammacTarget(vmecSurface::VmecSurface,zetaMin::Float64, zetaStep::Float64, zetaMax::Float64,
                      BResolution::Int64, epsEff::Bool=true, gammac::Bool=true)
  if epsEff && gammac
    targetValues = (Inf,Inf)
  elseif epsEff || gammac
    targetValues = Inf
  else
    println("must select either epsEff or gammac")
  end
  print("<---2Zeta goes from ", zetaMin, " to ", zetaMax, " in ", zetaStep, "-sized steps \n") 

  return ComputeGammac(vmecSurface,zetaMin,zetaStep,zetaMax,BResolution,epsEff,gammac) 
end

function GammacTarget(surfaces::Union{Float64,Vector{Float64}}, vmec::Vmec, zetaMin::Float64, 
                      zetaStep::Float64, zetaMax::Float64,
                      BResolution::Int64, comm::MPI.Comm, epsEff::Bool=true, gammac::Bool=true) 
  
  mpiRank=MPI.Comm_rank(comm)
  nTargetValues = length(surfaces)
  if epsEff && gammac
    targetValues = Array{Tuple{Float64, Float64}}(undef, nTargetValues)
  elseif epsEff || gammac
    targetValues = Array{Float64,1}(undef,nTargetValues)
  else
    println("must select either epsEff or gammac")
  end
  #println(nTargetValues)

  for index = 1:nTargetValues
    vmecSurface = VmecSurface(surfaces[index],vmec)

    targetTemp = ComputeGammac(vmecSurface,zetaMin, zetaStep, zetaMax, BResolution, comm,
                                        epsEff, gammac) 
    if mpiRank == 0
      targetValues[index] = targetTemp
    end
  end
  if mpiRank == 0
    return targetValues
  else
    return nothing
  end
end

function GammacTarget(vmecSurface::VmecSurface,zetaMin::Float64, zetaStep::Float64, zetaMax::Float64,
                      BResolution::Int64, comm::MPI.Comm, epsEff::Bool=true, gammac::Bool=true)
  if epsEff && gammac
    targetValues = (Inf,Inf)
  elseif epsEff || gammac
    targetValues = Inf
  else
    println("must select either epsEff or gammac")
  end

  return ComputeGammac(vmecSurface,zetaMin,zetaStep,zetaMax,BResolution,comm,epsEff,gammac) 
end

"""
    ComputeGammac(VmecSurface, zetaMin, zetaStep, zetaMax, BResolution, comm, epsEff, gammaC)
    ComputeGammac(VmecSurface, zetaMin, zetaStep, zetaMax, BResolution, epsEff, gammaC)

Helper function for GammacTarget  

# Arguments
 - `VmecSurface::VmecSurface`: A VmecSurface object, see documentation for xxx to be filled later xxx 
 - `zetaMin::Float64`: The toroidal angle value to start the calculation at, in radians
 - `zetaStep::Float64`: The step size to representing the resolution for the calculation, given in radians
 - `zetaMax::Float64`: The toroidal angle to end the calculation at, in radians
 - `BResolution::Int64`: The resolution for the B field integration
 - `comm:MPI.Comm`: MPI.Comm object. if present, the calculation will be parallelized
 - `epsEff::Bool=true`: Optional argument default true. if true, code will calculate and return the value of ``\\epsilon_\\mathrm{eff}``
 - `gammac::Bool=true`: Optional argument default true. if true, code will calculate and return the value of ``\\Gamma_c``

# Outputs:
 - `target::Vector{Float64}`: ``\\epsilon_\\mathrm{eff}`` and/or ``\\Gamma_c`` depending on the optional bool arguments. In the default case both are returned in the form in a pair of ``(\\epsilon_\\mathrm{eff}, \\Gamma_c)``
"""
function ComputeGammac(vmec::VmecSurface{T},
                       zetaMin::A,
                       zetaStep::A,
                       zetaMax::A,
                       BResolution::Int64,
                       comm:: MPI.Comm,
                       epsEff::Bool = true, gammac::Bool = true) where {T,A}

  #set up the needed MPI information
  mpiRank=MPI.Comm_rank(comm)
  mpiSize=MPI.Comm_size(comm)

  chunkBase = div(BResolution,mpiSize)
  chunkExtra = mod(BResolution,mpiSize)
  #construct the chunkSize and chunkStart arrays
  chunkStarts = Vector{Int64}(undef, mpiSize)
  chunkSizes = Vector{Int64}(undef, mpiSize)
  chunkStarts[1] = 1
  chunkSizes[1] = chunkBase
  if chunkExtra > 0
    chunkSizes[1] += 1
  end
  for i in 2:mpiSize
    chunkStarts[i] = chunkStarts[i- 1] + chunkSizes[i-1]
    chunkSizes[i] = i <= chunkExtra ? chunkBase + 1 : chunkBase
  end

  start = chunkStarts[mpiRank+1]
  chunkSize = chunkSizes[mpiRank+1]

# <<<<<<< HEAD
# 
#   #if mpiRank == 0
#   #  println("chunksizes: ",chunkSizes)
#   #  println("chunkstarts: ",chunkStarts)
#   #end
# 
#   zetaRange = zetaMin:zetaStep:zetaMax
#   zetas = collect(zetaRange)
#   dZeta = zetas[2]-zetas[1]
#   nZeta = length(zetas)
#   print("<---3Zeta goes from ", zetaMin, " to ", zetaMax, " in ", zetaStep, "-sized steps \n") 
# 
#   chiPrime = -vmec.iota[2] # Adjust sign for handedness of coordinate system
#   twoPiEdgeFlux = 2π/vmec.phi[end]/vmec.signgs
#   # Retrieve the necessary physical quantities from the VMEC equilibrium
#   Bmag = Vector{Float64}(undef,nZeta) 
#   geodesicCurvature = similar(Bmag)
#   eThetaMag = similar(Bmag)
#   gradPsiMag = similar(Bmag)
#   dBdPsi = similar(Bmag)
#   BsupZeta = similar(Bmag)
#   dBsupZetadPsi = similar(Bmag)
#   VT = similar(Bmag)
#   Bxyz = Vector{CoordinateVector{Float64}}(undef,nZeta)
#   grad_B = similar(Bxyz)
#   gradZeta = similar(Bxyz)
# 
#   #This can be parallelized also
#   looptime = @elapsed @inbounds for i = 1:nZeta
#     #Find the coordinates and perform coordinate transformations
#     pestCoords = PestCoordinates(vmec.s*vmec.phi[end]*vmec.signgs/(2*π),0.0,zetas[i])
#     vmecCoords = VmecFromPest()(pestCoords,vmec)
#     vmecContraBasis = contravariant_basis(CartesianFromVmec(),vmecCoords,vmec)
#     pestContraBasis = transform_basis(PestFromVmec(),vmecCoords,vmecContraBasis,vmec)
#     fluxContraBasis = transform_basis(FluxFromVmec(),vmecCoords,vmecContraBasis,vmec)
#     fluxCoBasis = transform_basis(CovariantFromContravariant(),fluxContraBasis)
# 
#     #Calculate the derived quantities
#     eThetaMag[i] = abs(fluxCoBasis,2)
#     gradPsiMag[i] = abs(pestContraBasis,1)
#     grad_B[i] = VMEC.gradB(vmecCoords,vmecContraBasis,vmec)
#     geodesicCurvature[i] = curvatureComponents(pestContraBasis,grad_B[i])[2]
#     Bmag[i] = inverseCosineTransform(vmecCoords,vmec.bmn)
#     dBdPsi[i] = twoPiEdgeFlux*inverseCosineTransform(vmecCoords,vmec.bmn;deriv=:ds)
#     BsupZeta[i] = inverseCosineTransform(vmecCoords,vmec.bsupvmn)
#     dBsupZetadPsi[i] = twoPiEdgeFlux*inverseCosineTransform(vmecCoords,vmec.bsupvmn;deriv=:ds)
#     gradZeta[i] = pestContraBasis[:,3]
#     Bxyz[i] = cross(pestContraBasis[:,1],pestContraBasis[:,2])
# 
#     #calculate the term in the parentheses in dVdb (Nem eq 35)
#     VT1 = chiPrime*dot(cross(pestContraBasis[:,1],Bxyz[i]),gradZeta[i])/(Bmag[i]*gradPsiMag[i])
#     VT2 = 2 * dBdPsi[i] - (Bmag[i] * dBsupZetadPsi[i]) / BsupZeta[i]
#     VT[i] = VT1 - VT2
#   end
#   #println("Time to retrieve data from VMEC: "*string(looptime))
#   
#   #calculate the relevant splines
#   Bspline = CubicSplineInterpolation(zetaRange, Bmag)
#   BsupZetaSpline = CubicSplineInterpolation(zetaRange, BsupZeta)
#   eThetaSpline = CubicSplineInterpolation(zetaRange, eThetaMag)
#   dBdPsiSpline = CubicSplineInterpolation(zetaRange, dBdPsi)
#   VTSpline = CubicSplineInterpolation(zetaRange, VT)
# 
#   #calculate all the extrema
#   zetaExtreme = allExtrema(Bspline,first(zetas),last(zetas), dZeta/2)
# 
#   # The ∇ψ vector is the contravariant basis vector w.r.t. to the first coordinate in PEST coordinates (ψ,α,ζ)
#   # This can be accessed by using PET.gradX1(pestVectors) and the magnitude computed with PET.abs()
#   # gradPsiMag has type Array{Float64,1}
#   gradPsiSpline = CubicSplineInterpolation(zetaRange, gradPsiMag)
#   kgSpline = CubicSplineInterpolation(zetaRange, geodesicCurvature)
#  
#   (Bmin, BTargetRange) = ConstructBTargetRange(Bmag, BResolution)
# 
#   #Integrate quantities over the full length for normalizations
#   dsOverB = IntdsOverB(BsupZetaSpline, zetaRange[1], zetaRange[end])
#   gradPsiOverB = IntGradPsiOverB(BsupZetaSpline, gradPsiSpline, zetaRange[1], zetaRange[end])
#  
# =======
# >>>>>>> wistell/GammaC.jl-master
  H2overI_b=Vector{Float64}(undef, BResolution)
  gammac_b=Vector{Float64}(undef, BResolution)
  bprime_b=Vector{Float64}(undef,BResolution)

  dsOverB, gradPsiOverB = ComputeGammac!(H2overI_b,gammac_b,bprime_b,
                                         vmec,zetaMin,zetaStep,zetaMax,
                                         BResolution,start,start+chunkSize-1)

  #Gather all the results
  if mpiRank == 0
    MPI.Gatherv!(MPI.IN_PLACE, VBuffer(H2overI_b,chunkSizes), 0, comm)
    MPI.Gatherv!(MPI.IN_PLACE, VBuffer(gammac_b,chunkSizes), 0, comm)
    MPI.Gatherv!(MPI.IN_PLACE, VBuffer(bprime_b,chunkSizes), 0, comm)
  else
    MPI.Gatherv!(H2overI_b[start:start+chunkSize-1],nothing,0,comm)
    MPI.Gatherv!(gammac_b[start:start+chunkSize-1],nothing,0,comm)
    MPI.Gatherv!(bprime_b[start:start+chunkSize-1],nothing,0,comm)
  end

  #println("Time to compute integral targets: "*string(looptime))
  MPI.Barrier(comm)
  
  if mpiRank > 0
    return nothing
  end

  if epsEff 
    H2overI = H2overIIntegral(bprime_b, H2overI_b)
    EpsEff32 = H2overI/(gradPsiOverB)^2*dsOverB * pi * vmec.Rmajor_p^2/8/sqrt(2)
    #println("epsfactors: ",vmec.Rmajor_p," ",dsOverB," ",gradPsiOverB)
    #println(EpsEff32," ",EpsEff32^(2.0/3))
  end

  if gammac 
    gammacInt = gammacIntegral(bprime_b, gammac_b)
    bigGammac = pi / 2 / sqrt(2) * gammacInt / dsOverB
  end

  if epsEff && gammac 
    target = (EpsEff32^(2.0/3), bigGammac)
  elseif epsEff
    target = EpsEff32^(2.0/3)
  else
    target = bigGammac
  end

  return target
end



#<<<<<<< HEAD
#  zetaRange = zetaMin:zetaStep:zetaMax
#   zetas = collect(zetaRange)
#   nZeta = length(zetas)
#   print("<---Zeta goes from ", zetaMin, " to ", zetaMax, " in ", zetaStep, "-sized steps \n") 
# 
#   chiPrime = -vmec.iota[2] # Adjust sign for handedness of coordinate system
#   twoPiEdgeFlux = 2π/vmec.phi[end]*vmec.signgs
#   # Retrieve the necessary physical quantities from the VMEC equilibrium
#   Bmag = Vector{Float64}(undef,nZeta) 
#   geodesicCurvature = similar(Bmag)
#   eThetaMag = similar(Bmag)
#   gradPsiMag = similar(Bmag)
#   dBdPsi = similar(Bmag)
#   BsupZeta = similar(Bmag)
#   dBsupZetadPsi = similar(Bmag)
#   VT = similar(Bmag)
#   VTx1 = similar(Bmag)
#   VTx2 = similar(Bmag)
#   Bxyz = Vector{CoordinateVector{Float64}}(undef,nZeta)
#   gradPsi = similar(Bxyz)
#   grad_B = similar(Bxyz)
#   gradZeta = similar(Bxyz)
#   print("<---Looping over 1:nZeta\n")
#   looptime = @elapsed @inbounds Threads.@threads for i = 1:nZeta
#     #Find the coordinates and perform coordinate transformations
#     pestCoords = PestCoordinates(vmec.signgs*vmec.phi[end]*vmec.s/(2*π),0.0,zetas[i])
#     vmecCoords = VmecFromPest()(pestCoords,vmec)
#     vmecContraBasis = contravariant_basis(CartesianFromVmec(),vmecCoords,vmec)
#     pestContraBasis = transform_basis(PestFromVmec(),vmecCoords,vmecContraBasis,vmec)
#     fluxContraBasis = transform_basis(FluxFromVmec(),vmecCoords,vmecContraBasis,vmec)
#     fluxCoBasis = transform_basis(CovariantFromContravariant(),fluxContraBasis)
# 
#     #Calculate the derived quantities
#     eThetaMag[i] = abs(fluxCoBasis,2)
#     gradPsi[i] = pestContraBasis[:,1]
#     gradPsiMag[i] = abs(pestContraBasis,1)
#     grad_B[i] = VMEC.gradB(vmecCoords,vmecContraBasis,vmec)
#     geodesicCurvature[i] = curvatureComponents(pestContraBasis,grad_B[i])[2]
#     Bmag[i] = inverseCosineTransform(vmecCoords,vmec.bmn)
#     dBdPsi[i] = twoPiEdgeFlux*inverseCosineTransform(vmecCoords,vmec.bmn;deriv=:ds)
#     BsupZeta[i] = inverseCosineTransform(vmecCoords,vmec.bsupvmn)
#     dBsupZetadPsi[i] = twoPiEdgeFlux*inverseCosineTransform(vmecCoords,vmec.bsupvmn;deriv=:ds)
#     gradZeta[i] = pestContraBasis[:,3]
#     Bxyz[i] = cross(pestContraBasis[:,1],pestContraBasis[:,2])
# 
#     #calculate the term in the parentheses in dVdb (Nem eq 35)
#     VT1 = chiPrime*dot(cross(pestContraBasis[:,1],Bxyz[i]),gradZeta[i])/(Bmag[i]*gradPsiMag[i])
#     VT2 = 2 * dBdPsi[i] - (Bmag[i] * dBsupZetadPsi[i]) / BsupZeta[i]
#     VT[i] = VT1 - VT2
#     VTx1[i] = VT1
#     VTx2[i] = VT2
#   end
#   #println("Time to retrieve data from VMEC: "*string(looptime))
#   print("<---iota=", chiPrime, "\n")
#   print("<---Finished with loop over 1:nZeta\n")
#   print("<---Writing to jgc1 file\n")
# 
#   open("jgc_scalars.txt", "w") do io
#            writedlm(io, [Bmag geodesicCurvature dBdPsi dBsupZetadPsi BsupZeta gradPsiMag eThetaMag VT VTx1 VTx2])
#        end
#   open("jgc_Bxyz.txt", "w") do io
#            writedlm(io, [Bxyz[:]], '\n')
#        end
#   open("jgc_grad_B.txt", "w") do io
#            writedlm(io, [grad_B[:]], '\n' )
#        end
#   open("jgc_gradPsi.txt", "w") do io
#            writedlm(io, [gradPsi[:]], '\n')
#        end
#   open("jgc_gradZeta.txt", "w") do io
#            writedlm(io, [gradZeta[:]], '\n')   
#        end
#   print("<---Finished writing additional jgc files\n")
# %=======
# %>>>>>>> wistell/GammaC.jl-master

function ComputeGammac(vmec::VmecSurface{T},
                       zetaMin::A,
                       zetaStep::A,
                       zetaMax::A,
                       BResolution::Int64,
                       epsEff::Bool = true,
                       gammac::Bool = true;
                      ) where {T,A}

  H2overI_b=Vector{T}(undef, BResolution)
  gammac_b=Vector{T}(undef, BResolution)
  bprime_b=Vector{T}(undef, BResolution)

# <<<<<<< HEAD
#   # The ∇ψ vector is the contravariant basis vector w.r.t. to the first coordinate in PEST coordinates (ψ,α,ζ)
#   # This can be accessed by using PET.gradX1(pestVectors) and the magnitude computed with PET.abs()
#   # gradPsiMag has type Array{Float64,1}
#   gradPsiSpline = CubicSplineInterpolation(zetaRange, gradPsiMag)
#   kgSpline = CubicSplineInterpolation(zetaRange, geodesicCurvature)
#  
#   (Bmin, BTargetRange) = ConstructBTargetRange(Bmag, BResolution)
# 
#   #Integrate quantities over the full length for normalizations
#   dsOverB = IntdsOverB(BsupZetaSpline, zetaRange[1], zetaRange[end])
#   gradPsiOverB = IntGradPsiOverB(BsupZetaSpline, gradPsiSpline, zetaRange[1], zetaRange[end])
#  
#   H2overI_b=Vector{Float64}(undef, BResolution)
#   gammac_b=Vector{Float64}(undef, BResolution)
#   bprime_b=Vector{Float64}(undef,BResolution)
# 
# 
#  # looptime = @elapsed Threads.@threads for bi = 1:BResolution
#   looptime = @elapsed Threads.@threads for bi = [Int(round(0.2*BResolution)), Int(round(0.7*BResolution))]
#     print("<---Doing bi=", bi, "\n")
#     Btarget = BTargetRange[bi]
#     #calculate locations of wells
#     wellBounds = calculateWells(zetaExtreme, Btarget, Bspline)
#     #print("<---wellBounds =\n", wellBounds)
#     #wellBounds returns a list of tuples of left bounds, right bounds and minima
#     gradPsiMin = map(w->gradPsiSpline(w[3]),wellBounds)
#     eThetaMin = map(w->eThetaSpline(w[3]),wellBounds)
# 
#     bprime = Btarget/Bmin
#     bprime_b[bi] = bprime
#     jgc_well_filename = string("jgc_wellbounds_", bi,".txt")
#     open(jgc_well_filename, "w") do io
#            writedlm(io, [wellBounds], '\n')   
#          end
# 
#     H2overI_b[bi], gammac_b[bi] = integralTargets(Bspline,BsupZetaSpline,gradPsiSpline,kgSpline,dBdPsiSpline,VTSpline,wellBounds,
#                                                   Bmin,bprime,gradPsiMin,eThetaMin)
#   end
#   #println("Time to compute integral targets: "*string(looptime))
#   print("<---finished with bi loop")
#  
# =======
 dsOverB, gradPsiOverB = ComputeGammac!(H2overI_b,gammac_b,bprime_b,
                                        vmec,zetaMin,zetaStep,zetaMax,
                                        BResolution,1,BResolution)

# >>>>>>> wistell/GammaC.jl-master
  if epsEff
    H2overI = H2overIIntegral(bprime_b, H2overI_b)
    EpsEff32 = H2overI/(gradPsiOverB)^2*dsOverB * pi * vmec.Rmajor_p^2/8/sqrt(2)
  end

  if gammac
    gammacInt = gammacIntegral(bprime_b, gammac_b)
    bigGammac = pi / 2 / sqrt(2) * gammacInt / dsOverB
  end

  if epsEff && gammac
    target = (EpsEff32^(2.0/3), bigGammac)
  elseif epsEff
    target = EpsEff32^(2.0/3)
  else
    target = bigGammac
  end
  return target
end

# <<<<<<< HEAD
# function ComputeGammac(vmecCoords::Vector{VmecCoordinates{T,A}},pestContraBasis::Vector{BasisVectors{T}},
#                        eThetaMag::Vector{Float64},geodesicCurvature::Vector{Float64},vmec::VmecSurface,
#                        BResolution::Int64,epsEff::Bool = true, gammac::Bool = true) where {T,A}
#   nZeta = length(vmecCoords)
#   dZeta = vmecCoords[2].ζ - vmecCoords[1].ζ
#   zetaRange = range(first(vmecCoords).ζ,step=dZeta,length=nZeta)
#   zeta = collect(zetaRange)
#   print("<---5Zeta goes from ", zetaMin, " to ", zetaMax, " in ", zetaStep, "-sized steps \n") 
# 
#   chiPrime = vmec.iota[2]
#   twoPiEdgeFlux = 2π/vmec.phi[end]
#   # Retrieve the necessary physical quantities from the VMEC equilibrium
#   Bmag = Vector{Float64}(undef,nZeta) 
#   dBdPsi = similar(Bmag)
#   BsupZeta = similar(Bmag)
#   dBsupZetadPsi = similar(Bmag)
#   VT1 = similar(Bmag)
#   Bxyz = Vector{CoordinateVector}(undef,nZeta)
#   gradZeta = similar(Bxyz)
#   @inbounds Threads.@threads for i = 1:nZeta
#     v = vmecCoords[i]
#     Bmag[i] = inverseCosineTransform(v,vmec.bmn)
#     dBdPsi[i] = twoPiEdgeFlux*inverseCosineTransform(v,vmec.bmn;deriv=:ds)
#     BsupZeta[i] = inverseCosineTransform(v,vmec.bsupvmn)
#     dBsupZetadPsi[i] = twoPiEdgeFlux*inverseCosineTransform(v,vmec.bsupvmn;deriv=:ds)
#     Bxyz[i] = cross(pestContraBasis[i][:,1],pestContraBasis[i][:,2])
#     gradZeta[i] = pestContraBasis[i][:,3]
#     VT1[i] = chiPrime*dot(cross(pestContraBasis[i][:,1],Bxyz[i]),gradZeta[i])
#   end
# 
#   #calculate the term in the parentheses in dVdb (Nem eq 35)
#   #VT1 = chiPrime .* PET.dot(PET.cross(PET.gradX1(pestVectors),Bxyz),gradZeta)
#   VT2 = 2 .* dBdPsi .- (Bmag .* dBsupZetadPsi) ./ BsupZeta
#   VT = VT1 - VT2
# =======
function ComputeGammac!(H2overI_b::AbstractVector{T},
                        gammac_b::AbstractVector{T},
                        bprime_b::AbstractVector{T},
                        vmec::VmecSurface{T},
                        zetaMin::A,
                        zetaStep::A,
                        zetaMax::A,
                        BResolution::Int,
                        firstIndex_b::Int,
                        lastIndex_b::Int,
                       ) where {T,A}
  
  zetaRange = zetaMin:zetaStep:zetaMax
# >>>>>>> wistell/GammaC.jl-master

  B, Bᵛ, dBdψ, ∇ψ, eθ, κ, Vₜ , Bmag = setupGammaCSplines(vmec,zetaRange)

  #calculate all the extrema
  zetaExtreme = allExtrema(B,first(zetaRange),last(zetaRange),step(zetaRange)/2)

  (Bmin, BTargetRange) = ConstructBTargetRange(Bmag, BResolution)

  #Integrate quantities over the full length for normalizations
  dsOverB = IntdsOverB(Bᵛ, zetaRange[1], zetaRange[end])
  gradPsiOverB = IntGradPsiOverB(Bᵛ, ∇ψ, zetaRange[1], zetaRange[end])
 
  #Loop over all the Btargets
  #for (bi, Btarget) in enumerate(BTargetRange)
  @batch for bi in firstIndex_b:lastIndex_b
    Btarget = BTargetRange[bi]
    #calculate locations of wells
    wellBounds = calculateWells(zetaExtreme, Btarget, B)

    jgc_well_filename = string("jgc_wellbounds_", bi,".txt")
    open(jgc_well_filename, "w") do io
           writedlm(io, [wellBounds], '\n')   
         end

    #wellBounds returns a list of tuples of left bounds, right bounds and minima
    gradPsiMin = map(w->∇ψ(w[3]),wellBounds)
    eThetaMin = map(w->eθ(w[3]),wellBounds)

    bprime = Btarget/Bmin
    bprime_b[bi] = bprime
    if (bi == 20) 
    H2overI_b[bi], gammac_b[bi] = integralTargets2(B,Bᵛ,∇ψ,κ,dBdψ,Vₜ,wellBounds,Bmin,bprime,gradPsiMin,eThetaMin)
    else
    H2overI_b[bi], gammac_b[bi] = integralTargets(B,Bᵛ,∇ψ,κ,dBdψ,Vₜ,wellBounds,Bmin,bprime,gradPsiMin,eThetaMin)
    end

  end

  return dsOverB, gradPsiOverB
end

