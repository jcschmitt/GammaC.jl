function setupGammaCSplines(vmec::VmecSurface{T},
                            zetaRange::AbstractRange;
                           ) where {T}
  zetas = collect(zetaRange)
  nZeta = length(zetas)

  chiPrime = -vmec.iota[2] # Adjust sign for handedness of coordinate system
  twoPiEdgeFlux = 2π/vmec.phi[end]*vmec.signgs
  # Retrieve the necessary physical quantities from the VMEC equilibrium
  Bmag = Vector{T}(undef,nZeta) 
  geodesicCurvature = similar(Bmag)
  normalCurvature = similar(Bmag)
  eThetaMag = similar(Bmag)
  gradPsiMag = similar(Bmag)
  dBdPsi = similar(Bmag)
  BsupZeta = similar(Bmag)
  dBsupZetadPsi = similar(Bmag)
  VT = similar(Bmag)
  VTx1 = similar(Bmag)
  VTx2 = similar(Bmag)
  Bxyz2 = Vector{CoordinateVector{Float64}}(undef,nZeta)
  pCoords = Vector{PestCoordinates}(undef,nZeta)
  vCoords = Vector{VmecCoordinates}(undef,nZeta)
  eTheta = similar(Bxyz2)
  gradPsi = similar(Bxyz2)
  gradZeta = similar(Bxyz2)
  grad_B2 = similar(Bxyz2)
  print("<---Looping over 1:nZeta\n")
  @inbounds for i = 1:nZeta
    #Find the coordinates and perform coordinate transformations
    pestCoords = PestCoordinates(vmec.signgs*vmec.phi[3]*vmec.s/(2*π),0.0,zetas[i])
    #print("<----pestCoords=",pestCoords,"\n")

    vmecCoords = VmecFromPest()(pestCoords,vmec)
    #print("<----vmecCoords=",vmecCoords,"\n")
    vmecContraBasis = basis_vectors(Contravariant(),CartesianFromVmec(),vmecCoords,vmec)
    pestContraBasis = transform_basis(PestFromVmec(),vmecCoords,vmecContraBasis,vmec)
    fluxContraBasis = transform_basis(FluxFromVmec(),vmecCoords,vmecContraBasis,vmec)
    fluxCoBasis = transform_basis(CovariantFromContravariant(),fluxContraBasis)

    pCoords[i] = pestCoords
    vCoords[i] = vmecCoords
    #Calculate the derived quantities
    eThetaMag[i] = abs(fluxCoBasis,2)
    eTheta[i] = fluxCoBasis[:,2]
    gradPsi[i] = pestContraBasis[:,1]
    gradZeta[i] = pestContraBasis[:,3]
    gradPsiMag[i] = abs(pestContraBasis,1)
    grad_B = gradB(vmecCoords,vmecContraBasis,vmec)
    grad_B2[i] = gradB(vmecCoords,vmecContraBasis,vmec)
    geodesicCurvature[i] = curvatureComponents(pestContraBasis,grad_B)[2]
    normalCurvature[i] = curvatureComponents(pestContraBasis,grad_B)[1]
    Bmag[i] = inverseTransform(vmecCoords,vmec.bmn)
    dBdPsi[i] = twoPiEdgeFlux*inverseTransform(vmecCoords,vmec.bmn,deriv=:ds)
    BsupZeta[i] = inverseTransform(vmecCoords,vmec.bsupvmn)
    dBsupZetadPsi[i] = twoPiEdgeFlux*inverseTransform(vmecCoords,vmec.bsupvmn,deriv=:ds)
    Bxyz = cross(pestContraBasis[:,1],pestContraBasis[:,2])
    Bxyz2[i] = cross(pestContraBasis[:,1],pestContraBasis[:,2])

    #calculate the term in the parentheses in dVdb (Nem eq 35)
    VT1 = chiPrime*dot(cross(pestContraBasis[:,1],Bxyz),gradZeta[i])/(Bmag[i]*gradPsiMag[i])
    VT2 = 2 * dBdPsi[i] - (Bmag[i] * dBsupZetadPsi[i]) / BsupZeta[i]
    VT[i] = VT1 - VT2
    VTx1[i] = VT1
    VTx2[i] = VT2
  end

   print("<---iota=", chiPrime, "\n")
   print("<---Finished with loop over 1:nZeta\n")
   print("<---Writing to jgc1 file\n")
 
   open("jgc_scalars.txt", "w") do io
            writedlm(io, [zetas Bmag geodesicCurvature dBdPsi dBsupZetadPsi BsupZeta gradPsiMag eThetaMag VT VTx1 VTx2 normalCurvature])
        end
   open("jgc_eTheta.txt", "w") do io
            writedlm(io, [eTheta[:]], '\n')
        end
   open("jgc_Bxyz.txt", "w") do io
            writedlm(io, [Bxyz2[:]], '\n')
        end
   open("jgc_pCoords.txt", "w") do io
            writedlm(io, [pCoords[:]], '\n')
        end
   open("jgc_vCoords.txt", "w") do io
            writedlm(io, [vCoords[:]], '\n')
        end
   open("jgc_grad_B.txt", "w") do io
            writedlm(io, [grad_B2[:]], '\n' )
        end
   open("jgc_gradPsi.txt", "w") do io
            writedlm(io, [gradPsi[:]], '\n')
        end
   open("jgc_gradZeta.txt", "w") do io
            writedlm(io, [gradZeta[:]], '\n')   
        end
   print("<---Finished writing additional jgc files\n")

  #calculate the relevant splines
  Bspline = CubicSplineInterpolation(zetaRange, Bmag)
  BsupZetaSpline = CubicSplineInterpolation(zetaRange, BsupZeta)
  eThetaSpline = CubicSplineInterpolation(zetaRange, eThetaMag)
  dBdPsiSpline = CubicSplineInterpolation(zetaRange, dBdPsi)
  VTSpline = CubicSplineInterpolation(zetaRange, VT)


  # The ∇ψ vector is the contravariant basis vector w.r.t. to the first coordinate in PEST coordinates (ψ,α,ζ)
  gradPsiSpline = CubicSplineInterpolation(zetaRange, gradPsiMag)
  kgSpline = CubicSplineInterpolation(zetaRange, geodesicCurvature)

  return Bspline,BsupZetaSpline,dBdPsiSpline,gradPsiSpline,eThetaSpline,kgSpline,VTSpline, Bmag
end


function residual(x::T,
                  target::T,
                  spline::Interpolations.Extrapolation;
                 ) where {T}
  return target - spline(x)
end

"""
    calculateWells(zetaExtreme, Btarget, Bspline)

Calculates the well boundaries given a set of extrema, the spline of the magnetic field and a target field. This is all points along a field line where the field strength is equivalent to the target field strength

#Arguments

 - `zetaExtreme::Vector{Float64}`: A vector given all the extrema (minima and maxima) of points along a field line.  This must be 100% accurate or the integrations may fail
 - `Btarget::Float64`: The target field 
 - `Bspline::Interpolations.Extrapolation`: The spline representation of the magnetic field

#Outputs
 - `wellBounds::Vector{Tuple}`: The output is a list of well boundaries each of which is a tuple of the form:
(``\\zeta`` value of the left boundary, ``\\zeta`` of right boundary, ``\\zeta`` of minimum)

The code works by finding pairs of extrema where one is larger than the target and the other is smaller than the target. The crossing points are found using the `find_zeros` function. Then the points are assembled into a well tuple where the left and right boundaries are present along with the ``\\zeta`` value of the field minimum which is also used in the ``\\Gamma_c`` calculation
"""
function calculateWells(zetaExtreme::AbstractVector{T},
                        Btarget::T,
                        Bspline::Interpolations.Extrapolation;
                       ) where {T}
  
  lbound = 0
  rbound = 0
  curBmin = Btarget
  curzmin = 0
  Bprev = Bspline(zetaExtreme[1])
  wellBounds = Tuple{T,T,T}[]
  
  #go through all the extrema
  for i in 2:length(zetaExtreme)
    Bcur = Bspline(zetaExtreme[i])

    #we hit a left side boundary
    if Bprev > Btarget && Bcur < Btarget
      lbound = find_zeros(x->residual(x,Btarget,Bspline),zetaExtreme[i-1],zetaExtreme[i])[1]
      curzmin = zetaExtreme[i]
      curBmin = Bspline(curzmin)

    #check if we have a minimum
    elseif lbound != 0 && Bcur < curBmin
      curBmin = Bcur
      curzmin = zetaExtreme[i]
    end

    #we hit a right side boundary, store the well
    if lbound != 0 && Bprev < Btarget && Bcur > Btarget
      rbound = find_zeros(x->residual(x,Btarget,Bspline),zetaExtreme[i-1],zetaExtreme[i])[1]
      wellInfo = (lbound, rbound, curzmin)
      push!(wellBounds, wellInfo)
      lbound = 0
    end
    Bprev = Bcur
  end
  return wellBounds
end

#Function to calculate wells- this algorithm was not 100% accurate
#Left here as a warning in case we try something like this again.
function calculateWellsOld(zeta::AbstractRange,
                           Btarget::T,
                           Bspline::Interpolations.Extrapolation;
                          ) where {T}

  wellCrosses = find_zeros(x->residual(x,Btarget,Bspline), first(zeta),last(zeta))
  nWellCrosses = length(wellCrosses)
 
  # Applying the sign() function to the derivative of B at each of the crossing points
  # will yield a structure [...,-1,1,-1,1,...] where each (-1,1) pair is the lower and upper
  # bound of the well.  If the fieldline starts in a well, the first value will be one
  # and if ends in a well, the value will be -1 and those values can be discarded
  startInWell = sign(Interpolations.gradient(Bspline,wellCrosses[1])[1]) == 1 ? true : false
  endInWell = sign(Interpolations.gradient(Bspline,wellCrosses[nWellCrosses])[1]) == -1 ? true : false
  nWells = div(nWellCrosses-startInWell-endInWell,2)

  # Associate the two well bounds to each other in an iterable container
  wellBounds = Vector{Pair{Float64,Float64}}(undef,nWells)
  map!(i->Pair(wellCrosses[i],wellCrosses[i+1]),wellBounds,range(1+startInWell,step=2,length=nWells))

  return wellBounds
  
  #this algorithm sometimes misses zeros which is why it was replaced
  #can salvage to implement a search routine to find the missing zero (but not worth)
end

"""
    splineMinimum(spline, x1, x2)
    splineMinimum(spline, xPair)

calculate the minimum of a spline between x1 and x2 (or between xPair[1] and xPair[2])
It can handle multiple minima, in which case it will return the smallest

#Arguments:

 - `spline::Interpolations.Extrapolation`: the spline object
 - `x1::Float64`: the lower bound
 - `x2::Float64`: the upper bound
 - `xPair::Pair{Float64, Float64}`: A pair of floats giving the upper and lower bound

#Outputs

 - `minimum::Float64::` The minimal value

"""
function splineMinimum(spline::Interpolations.Extrapolation,
                       x1::T,
                       x2::T;
                      ) where {T}
  function splineDeriv(x::T)
    return Interpolations.gradient(spline,x)[1]
  end
  candidateMinima = find_zeros(splineDeriv, x1, x2)
  indMinimum = argmin(spline.(candidateMinima))
  return minimum(candidateMinima)
end

function splineMinimum(spline::Interpolations.Extrapolation,
                       xPair::Pair{T,T};
                      ) where {T}
  return splineMinimum(spline,xPair.first,xPair.second)
end

"""
    allExtrema(spline, x1, x2, dx)

Calculate and return all extremal values of the spline between x1 and x2

#Arguments
 - `spline::Interpolations.Extrapolation`: The spline object to calculate extrema of
 - `x1::Float64`: The lower bound
 - `x2::Float64`: The upper bound
 - `dx::Float64`: Th minimum value that needs to be considered that can have two extrema. For a cubic spline with the smallest distance of knots = ``\\delta x`` this value should be no more than ``\\delta x/2``.

#Outputs
 - `extrema::Vector::Float64` A list of extrema ordered by the position along the spline

Previous attempts at calculation used canned routines that sometimes missed extrema. The current version is slow and methodical but will not miss any extrema. It considers spaces on the spline of dx apart and looks for changes in the derivative. If any changes are found it calculates the maxima and the minima. If dx is too large and a maximum and a mininum occurs, those values will not be found. So dx must be suitably small.
"""
function allExtrema(spline::Interpolations.Extrapolation,
                    x1::T,
                    x2::T,
                    dx::T;
                   ) where {T}
  function splineDeriv(x::T)
    return Interpolations.gradient(spline,x)[1]
  end
  #Finding all the zeros very very occasionally misses one, so let's do it manually
  prev = x1
  signPrev = sign(splineDeriv(x1))
  curr = x1+dx
  extrema = T[]
  while curr < x2
    signCurr = sign(splineDeriv(curr))
    if signPrev * signCurr == -1
      val = find_zeros(splineDeriv,prev,curr)[1]
      push!(extrema, val)
    end
    prev = curr
    signPrev = signCurr
    curr += dx
  end
  return extrema
end

"""
    ConstructBTargetRange(Bmag, BResolution)

Generate the target array (the ``db'`` integral in eq 61 in Nemov PoP 2008

#Arguments
 - `Bmag::Vector{Float64}`: The array of knots used to generate the Bspline
 - `Bresolution::Int64`: The number of points to use in the integral

Take ``|B|`` and the Resolution, return an array of values
which represent the BTarget values to compute the integrands at
Also return BMin which is one stepsize below the lowest value

Todo: replace Bmag with the array of extrema.
"""
function ConstructBTargetRange(Bmag::AbstractVector{T},
                               BResolution::Int;
                              ) where {T}
  #Note that the minimum and maximum shouldn't need to be terribly accurate here
  Bmin = minimum(Bmag)
  Bmax = maximum(Bmag)
  Bstep = (Bmax - Bmin)/(BResolution+1)
  #Make sure we don't get small errors in computing the range
  #tol = 1.0E-6 # JCS comment: should maybe check that tol < Bstep
  tol = convert(T,1.0E-6)
  BTargetRange = Bmin+Bstep-tol:Bstep:Bmax-Bstep+tol

  return Bmin, BTargetRange
end


#calculate the values of zeta where each well has a minimum
function wellMinimaLocations(Bspline::Interpolations.Extrapolation,
                             wellBounds::AbstractVector{Pair{T,T}};
                            ) where {T}
  return map(well->splineMinimum(Bspline,well),wellBounds)
end

"""
    dIdBIntegrand(x,Bspline,BsupZetaSpline,Bmin,bprime)

calculate ``d\\hat{I}/dB`` adapated from eq36 in Nemov PoP 2008.  Note that the integrand is over ``d\\zeta`` not ``ds`` requiring the conversion ``ds = \\frac{B d\\zeta }{B^\\zeta}``
"""
function dIdBIntegrand(x::T,
                       Bspline::Interpolations.Extrapolation,
                       BsupZetaSpline::Interpolations.Extrapolation,
                       Bmin::T,
                       bprime::T;
                      ) where {T}
  return 0.5*Bspline(x)/BsupZetaSpline(x)/Bmin/bprime/bprime/sqrt(1-Bspline(x)/Bmin/bprime)
end

"""
    dgdBIntegrand(x,Bspline,BsupZetaSpline,gradψ,κ,Bmin,bprime)

calculate ``dg/dB`` adapated from eq49 in Nemov PoP 2008.  Note that the integrand is over ``d\\zeta`` not ``ds`` requiring the conversion ``ds = \\frac{B d\\zeta }{B^\\zeta}``
"""
function dgdBIntegrand(x::T,
                       Bspline::Interpolations.Extrapolation,
                       BsupZetaSpline::Interpolations.Extrapolation,
                       gradψ::Interpolations.Extrapolation,
                       κ::Interpolations.Extrapolation,
                       Bmin::T,
                       bprime::T;
                      ) where {T}
  t1 = 0.5*gradψ(x)*κ(x)/BsupZetaSpline(x)/(bprime^2)
  t2 = sqrt(1 - Bspline(x)/Bmin/bprime)
  return t1*(t2 + 1/t2)
end

"""
    dGdBIntegrand(x,Bspline,BsupZetaSpline,dBdPsiSpline,Bmin,bprime)

calculate ``d\\hat{G}/dB`` adapated from eq29 in Nemov PoP 2008.  Note that the integrand is over ``d\\zeta`` not ``ds`` requiring the conversion ``ds = \\frac{B d\\zeta }{B^\\zeta}``
"""
function dGdBIntegrand(x::T,
                       Bspline::Interpolations.Extrapolation,
                       BsupZetaSpline::Interpolations.Extrapolation,
                       dBdPsiSpline::Interpolations.Extrapolation,
                       Bmin::T,
                       bprime::T;
                      ) where {T}
  t1 = 0.5*dBdPsiSpline(x)/BsupZetaSpline(x)/Bmin/(bprime^2)
  t2 = sqrt(1 - Bspline(x)/Bmin/bprime)
  return t1*(t2 + 1.0/t2)
end

"""
    dVdBIntegrand(x,Bspline,BsupZetaSpline,VTSpline,Bmin,bprime)

calculate ``d\\hat{V}/dB`` adapated from eq30 in Nemov PoP 2008.  Note that the integrand is over ``d\\zeta`` not ``ds`` requiring the conversion ``ds = \\frac{B d\\zeta }{B^\\zeta}``
"""
function dVdBIntegrand(x::T,
                       Bspline::Interpolations.Extrapolation,
                       BsupZetaSpline::Interpolations.Extrapolation,
                       VTSpline::Interpolations.Extrapolation,
                       Bmin::T,
                       bprime::T;
                      ) where {T}
  t1 = 1.5*VTSpline(x)/BsupZetaSpline(x)/Bmin/(bprime^2)
  t2 = sqrt(1 - Bspline(x)/Bmin/bprime)
  return t1*t2
end 

""" 
    IIntegrand(x,Bspline,BsupZetaSpline,Bmin,bprime)

calculate ``d\\hat{I}/dB`` from eq31 in Nemov PoP 1999.  Note that the integrand is over ``d\\zeta`` not ``ds`` requiring the conversion ``ds = \\frac{B d\\zeta }{B^\\zeta}``
"""
function IIntegrand(x::T,
                    Bspline::Interpolations.Extrapolation,
                    BsupZetaSpline::Interpolations.Extrapolation,
                    Bmin::T,
                    bprime::T;
                   ) where {T}
  return 1.0/BsupZetaSpline(x)*sqrt(1-Bspline(x)/Bmin/bprime)
end

"""
    HIntegrand(x,Bspline,BsupZetaSpline,gradPsiSpline,kgSpline,Bmin,bprime)

calculate ``d\\hat{H}/dB`` from eq30 in Nemov PoP 1999.  Note that the integrand is over ``d\\zeta`` not ``ds`` requiring the conversion ``ds = \\frac{B d\\zeta }{B^\\zeta}``
"""
function HIntegrand(x::T,
                    Bspline::Interpolations.Extrapolation,
                    BsupZetaSpline::Interpolations.Extrapolation,
                    gradPsiSpline::Interpolations.Extrapolation,
                    kgSpline::Interpolations.Extrapolation,
                    Bmin::T,
                    bprime::T) where {T}
  t1 = 1.0/BsupZetaSpline(x)/bprime
  #t2 = sqrt(complex(bprime - Bspline(x)/Bmin))
  t2 = sqrt(bprime - Bspline(x)/Bmin)
  t3 = 4.0*Bmin/Bspline(x) - 1.0/bprime
  t4 = gradPsiSpline(x) * kgSpline(x)
  return t1*t2*t3*t4
end

"""
    integralTargets(Bspline, BsupZetaSpline, gradPsiSpline, kappaSpline, dBdPsiSpline, VTSpline, wellBounds, Bmin, bprime, gradPsiMin, eThetaMin)

#Arguments
 - `Bspline::Interpolations.Extrapolations`: Spline of ``|B|``
 - `BsupZetaSpline::Interpolations.Extrapolations`: Spline of ``B^\\zeta``
 - `gradPsiSpline::Interpolations.Extrapolations`: Spline of ``d\\psi/ds``
 - `kappaSpline::Interpolations.Extrapolations`: Spline of geodesic curvature, ``\\kappa_g``
 - `dBdpsiSpline::Interpolations.Extrapolations`: Spline of ``dB/d\\psi``
 - `VTSpline::Interpolations.Extrapolations`: Spline of ``\\chi' \\left( \\nabla \\psi \\times \\mathbf{b}\\right) \\cdot \\nabla \\zeta \\left(2 \\frac{dB}{d\\psi} - \\frac{B}{B^\\zeta}\\frac{dB^\\zeta}{d\\psi}\\right)``
 - `wellBounds::Vector{Any}`: List of well boundaries
 - `Bmin::Float64`: Minimum value of ``|B|`` on the surface, used as ``B_0`` in Nemov equations
 - `bprime::Float64`: inegration parameter, equivalent to ``B\\mathrm{target}/B_0``
 - `gradPsiMin::Vector{Float64}`: Values of ``|\\nabla \\psi|`` evaluated at the well minima
 - `eThetaMin::Vector{Float64}`: Values of ``|\\mathbf{e}_\\theta|`` evaluated at the well minima

#Outputs
 - `epsEffValue::Float64`: Value of the integrand for ``\\epsilon_\\mathrm{eff}`` summed over all wells evaluated at ``b'``
 - `gammacValue::Float64`: Value of the integrand for ``\\Gamma_c`` summed over all wells evaluated at ``b'``

The function returns the main quantities needed for evaluating ``\\epsilon_\\mathrm{eff}`` and ``\\Gamma_c`` for a given value of the integration parameter ``b'``. The integrations are done slightly inside the well boundaries to avoid computational errors for the quantities with integrable infinities at the boundaries.

"""
function integralTargets(Bspline::Interpolations.Extrapolation, 
                         BsupZetaSpline::Interpolations.Extrapolation,
                         gradPsiSpline::Interpolations.Extrapolation,
                         kappaSpline::Interpolations.Extrapolation,
                         dBdPsiSpline::Interpolations.Extrapolation,
                         VTSpline::Interpolations.Extrapolation,
                         wellBounds::AbstractVector{Tuple{T,T,T}},
                         Bmin::T,
                         bprime::T,
                         gradPsiMin::AbstractVector{T},
                         eThetaMin::AbstractVector{T};
                        ) where {T}
  #epsEffValues = Vector{T}(undef,length(wellBounds))
  epsEffValues = similar(gradPsiMin)
  gammacValues = similar(gradPsiMin)
  for i = 1:length(wellBounds)
    lbound = wellBounds[i][1]+1e5*eps()
    rbound = wellBounds[i][2]-1e5*eps()
    dIdBValue = quadde(x->dIdBIntegrand(x,Bspline,BsupZetaSpline,Bmin,bprime),lbound,rbound)[1]
    dgdBValue = quadde(x->dgdBIntegrand(x,Bspline,BsupZetaSpline,gradPsiSpline,kappaSpline,Bmin,bprime),lbound,rbound)[1]
    dGdBValue = quadde(x->dGdBIntegrand(x,Bspline,BsupZetaSpline,dBdPsiSpline,Bmin,bprime),lbound,rbound)[1]
    dVdBValue = quadde(x->dVdBIntegrand(x,Bspline,BsupZetaSpline,VTSpline,Bmin,bprime),lbound,rbound)[1]
    IValue = quadde(x->IIntegrand(x,Bspline,BsupZetaSpline,Bmin,bprime),lbound,rbound)[1]
    HValue = quadde(x->HIntegrand(x,Bspline,BsupZetaSpline,gradPsiSpline,kappaSpline,Bmin,bprime),lbound,rbound)[1]
    @inbounds epsEffValues[i] = HValue * HValue / IValue
    @inbounds gammacValues[i] = (2/π*atan((dgdBValue/gradPsiMin[i]/dIdBValue)/(Bmin*eThetaMin[i]*(dGdBValue/dIdBValue + 2/3*dVdBValue/dIdBValue))))^2 * dIdBValue
  end

  return sum(epsEffValues), sum(gammacValues)
end

"""
    integralTargets2(Bspline, BsupZetaSpline, gradPsiSpline, kappaSpline, dBdPsiSpline, VTSpline, wellBounds, Bmin, bprime, gradPsiMin, eThetaMin)

#Arguments
 - `Bspline::Interpolations.Extrapolations`: Spline of ``|B|``
 - `BsupZetaSpline::Interpolations.Extrapolations`: Spline of ``B^\\zeta``
 - `gradPsiSpline::Interpolations.Extrapolations`: Spline of ``d\\psi/ds``
 - `kappaSpline::Interpolations.Extrapolations`: Spline of geodesic curvature, ``\\kappa_g``
 - `dBdpsiSpline::Interpolations.Extrapolations`: Spline of ``dB/d\\psi``
 - `VTSpline::Interpolations.Extrapolations`: Spline of ``\\chi' \\left( \\nabla \\psi \\times \\mathbf{b}\\right) \\cdot \\nabla \\zeta \\left(2 \\frac{dB}{d\\psi} - \\frac{B}{B^\\zeta}\\frac{dB^\\zeta}{d\\psi}\\right)``
 - `wellBounds::Vector{Any}`: List of well boundaries
 - `Bmin::Float64`: Minimum value of ``|B|`` on the surface, used as ``B_0`` in Nemov equations
 - `bprime::Float64`: inegration parameter, equivalent to ``B\\mathrm{target}/B_0``
 - `gradPsiMin::Vector{Float64}`: Values of ``|\\nabla \\psi|`` evaluated at the well minima
 - `eThetaMin::Vector{Float64}`: Values of ``|\\mathbf{e}_\\theta|`` evaluated at the well minima

#Outputs
 - `epsEffValue::Float64`: Value of the integrand for ``\\epsilon_\\mathrm{eff}`` summed over all wells evaluated at ``b'``
 - `gammacValue::Float64`: Value of the integrand for ``\\Gamma_c`` summed over all wells evaluated at ``b'``

The function returns the main quantities needed for evaluating ``\\epsilon_\\mathrm{eff}`` and ``\\Gamma_c`` for a given value of the integration parameter ``b'``. The integrations are done slightly inside the well boundaries to avoid computational errors for the quantities with integrable infinities at the boundaries.

"""
function integralTargets2(Bspline::Interpolations.Extrapolation, 
                         BsupZetaSpline::Interpolations.Extrapolation,
                         gradPsiSpline::Interpolations.Extrapolation,
                         kappaSpline::Interpolations.Extrapolation,
                         dBdPsiSpline::Interpolations.Extrapolation,
                         VTSpline::Interpolations.Extrapolation,
                         wellBounds::AbstractVector{Tuple{T,T,T}},
                         Bmin::T,
                         bprime::T,
                         gradPsiMin::AbstractVector{T},
                         eThetaMin::AbstractVector{T};
                        ) where {T}
  #epsEffValues = Vector{T}(undef,length(wellBounds))
  epsEffValues = similar(gradPsiMin)
  gammacValues = similar(gradPsiMin)
  for i = 1:length(wellBounds)
    lbound = wellBounds[i][1]+1e5*eps()
    rbound = wellBounds[i][2]-1e5*eps()
    dIdBValue = quadde(x->dIdBIntegrand(x,Bspline,BsupZetaSpline,Bmin,bprime),lbound,rbound)[1]
    dgdBValue = quadde(x->dgdBIntegrand(x,Bspline,BsupZetaSpline,gradPsiSpline,kappaSpline,Bmin,bprime),lbound,rbound)[1]
    dGdBValue = quadde(x->dGdBIntegrand(x,Bspline,BsupZetaSpline,dBdPsiSpline,Bmin,bprime),lbound,rbound)[1]
    dVdBValue = quadde(x->dVdBIntegrand(x,Bspline,BsupZetaSpline,VTSpline,Bmin,bprime),lbound,rbound)[1]
    IValue = quadde(x->IIntegrand(x,Bspline,BsupZetaSpline,Bmin,bprime),lbound,rbound)[1]
    HValue = quadde(x->HIntegrand(x,Bspline,BsupZetaSpline,gradPsiSpline,kappaSpline,Bmin,bprime),lbound,rbound)[1]
    @inbounds epsEffValues[i] = HValue * HValue / IValue
    @inbounds gammacValues[i] = (2/π*atan((dgdBValue/gradPsiMin[i]/dIdBValue)/(Bmin*eThetaMin[i]*(dGdBValue/dIdBValue + 2/3*dVdBValue/dIdBValue))))^2 * dIdBValue

   mydgdB = similar(gradPsiMin, 21)
   mydgdB_p1 = similar(gradPsiMin, 21)
   mydgdB_p2 = similar(gradPsiMin, 21)
   mydgdB_p3 = similar(gradPsiMin, 21)
   mydgdB_p4 = similar(gradPsiMin, 21)
   mydgdB_p5 = similar(gradPsiMin, 21)
   mydIdB = similar(gradPsiMin, 21)
   mydGdB = similar(gradPsiMin, 21)
   mydVdB = similar(gradPsiMin, 21)
   myxout = similar(gradPsiMin, 21)
   myxrange = collect(LinRange(lbound, rbound, 21)) 
   for jj = 1:21
     myxout[jj] = myxrange[jj]
     mydgdB[jj] = dgdBIntegrand(myxrange[jj],Bspline,BsupZetaSpline,gradPsiSpline,kappaSpline,Bmin,bprime)
     mydgdB_p1[jj] = gradPsiSpline(myxrange[jj])
     mydgdB_p2[jj] = kappaSpline(myxrange[jj])
     mydgdB_p3[jj] = BsupZetaSpline(myxrange[jj])
     mydgdB_p4[jj] = bprime^2 
     mytemp = sqrt(1 - Bspline(myxrange[jj]) / Bmin / bprime)
     mydgdB_p5[jj] = mytemp + 1/mytemp
     mydIdB[jj] = dIdBIntegrand(myxrange[jj],Bspline,BsupZetaSpline,Bmin,bprime)
     mydGdB[jj] = dGdBIntegrand(myxrange[jj],Bspline,BsupZetaSpline,dBdPsiSpline,Bmin,bprime)
     mydVdB[jj] = dVdBIntegrand(myxrange[jj],Bspline,BsupZetaSpline,VTSpline,Bmin,bprime)
   end

   jgc_wellInt_filename = string("jgc_wellint_", i,".txt")
   open(jgc_wellInt_filename, "w") do io
          writedlm(io, [lbound rbound dIdBValue dgdBValue dGdBValue dVdBValue IValue HValue gammacValues[i]], '\n')   
        end

   jgc_filename_d = string("jgc_wellint_x_", i,".txt")
   open(jgc_filename_d, "w") do io
          writedlm(io, [myxout mydIdB mydgdB mydGdB mydVdB])   
        end
 
   jgc_filename_dg = string("jgc_wellint_dgdB_subparts_", i,".txt")
   open(jgc_filename_dg, "w") do io
          writedlm(io, [myxout mydgdB_p1 mydgdB_p2 mydgdB_p3 mydgdB_p4 mydgdB_p5])   
        end
 
  end

  return sum(epsEffValues), sum(gammacValues)
end



"""
    IntdsOverB(BsupZetaSpline, zmin, zmax)

Integrate dsOverB bw zmin and zmax, used in epseff and gammac

#Arguments

 - `BsupZetaSpline::Interpolations.Extrapolation`: Spline of ``B^\\zeta``
 - `zmin::Float64`: minimum value of ``\\zeta``
 - `zmax::Float64`: maximum value of ``\\zeta``

#Outputs
 - value of ``int_{\\zeta_\\mathrm{min}}^{\\zeta_\\mathrm{max}} ds/|B|``

calculate ``\\int ds/|B|``.  Note that the integrand is over ``d\\zeta`` not ``ds`` requiring the conversion ``ds = \\frac{B d\\zeta }{B^\\zeta}``
dzeta/BsupZeta
"""
function IntdsOverB(BsupZetaSpline::Interpolations.Extrapolation,
                    zmin::T,
                    zmax::T;
                   ) where {T}
  function dsOverBIntegrand(x::T)

    return 1.0/BsupZetaSpline(x)
  end 
  
  function dsOverBIntegral(f::Function,
                           a::T,
                           b::T,
                           delta::T;
                          ) where {T}
    try
      return quadgk(f,a+delta,b-delta)[1]
    catch DomainError
      return dsOverBIntegral(f,a,b,10*delta)
    end
  end

  return dsOverBIntegral(dsOverBIntegrand, zmin, zmax, eps(T))
end

"""
    IntGradPsiOverB(BsupZetaSpline, gradPsiSpline, zmin, zmax)

#Arguments

 - `BsupZetaSpline::Interpolations.Extrapolation`: Spline of ``B^\\zeta``
 - `gradPsiSpline::Interpolations.Extrapolation`: Spline of ``|\\nabla \\psi|``
 - `zmin::Float64`: minimum value of ``\\zeta``
 - `zmax::Float64`: maximum value of ``\\zeta``

#Outputs
 - value of ``int_{\\zeta_\\mathrm{min}}^{\\zeta_\\mathrm{max}} |\\nabla \\psi|ds``

calculate ``\\int |\\nabla \\psi|ds``.  Note that the integrand is over ``d\\zeta`` not ``ds`` requiring the conversion ``ds = \\frac{B d\\zeta }{B^\\zeta}``

"""
function IntGradPsiOverB(BsupZetaSpline::Interpolations.Extrapolation,
                         gradPsiSpline::Interpolations.Extrapolation,
                         zmin::T,
                         zmax::T;
                        ) where {T}
  function GradPsiOverBIntegrand(x::T)

    return gradPsiSpline(x)/BsupZetaSpline(x)
  end 
  
  function GradPsiOverBIntegral(f::Function,
                                a::T,
                                b::T,
                                delta::T;
                               )
    try
      return quadgk(f,a+delta,b-delta)[1]
    catch DomainError
      return GradPsiOverBIntegral(f,a,b,10*delta)
    end
  end

  return GradPsiOverBIntegral(GradPsiOverBIntegrand, zmin, zmax, eps(T))
end


"""
    H2overIIntegral(b, H2overI)

Solve ``\\int \\frac{\\hat{H}^2}{\\hat{I}}db'``, the key component for ``\\epsilon_\\mathrm{eff}``

#Arguments

 - `b::Vector{Float64}`: values of ``b'``
 - `H2overI::Vector{Float64}`: values of ``\\frac{\\hat{H}^2}{\\hat{I}``

#Outputs

 - `ans:Float64`: The value of the integral
"""
function H2overIIntegral(b::AbstractVector{T},
                         H2overI::AbstractVector{T};
                        ) where T
  
  bRange = b[1]:b[2]-b[1]:b[end]+b[2]/2 - b[1]/2
  H2overISpline = CubicSplineInterpolation(bRange, H2overI)
   
  function H2overIIntegrand(x)
    return H2overISpline(x)
  end
  return quadgk(H2overIIntegrand, b[1], b[end])[1]
end

"""
    gammacIntegral(b, gammac_b)

Solve ``\\int \\gamma_c^2 \\frac{\\partial \\hat{I}}{\\partial b'}db'``, the key component for ``\\Gamma_c``

#Arguments

 - `b::Vector{Float64}`: values of ``b'``
 - `gammac_b::Vector{Float64}`: values of ``\\gamma_c^2 \\frac{\\partial \\hat{I}}{\\partial b'}``

#Outputs:

 - `ans::Float64`: The value of the integral

Note that the input parameter is equal to ``\\gamma_c^2 \\frac{\\partial \\hat{I}}{\\partial b'}`` where ``\\gamma_c = \\frac{2}{\\pi} \\mathrm{tan}^{-1} \\frac{v_\\mathrm{an}}{\\hat{v}_\\theta}``  See equations 50, 51 and 61 in Nemov PoP 2008
"""
function gammacIntegral(b::AbstractVector{T},
                        gammac_b::AbstractVector{T};
                       ) where {T}
  
  bRange = b[1]:b[2]-b[1]:b[end]+b[2]/2 - b[1]/2
  gammacSpline = Interpolations.CubicSplineInterpolation(bRange, gammac_b)
   
  function gammacIntegrand(x)
    return gammacSpline(x)
  end
  return quadgk(gammacIntegrand, b[1], b[end])[1]
end
